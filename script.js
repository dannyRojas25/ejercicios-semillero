var listInfo = [
	{
		'id': 1,
		'nombre': 'Juan José',
		'edad': '26',
		'telefono': '3245678'
	},
	{
		'id': 2,
		'nombre': 'Ana María',
		'edad': '31',
		'telefono' : '3142145'
	}
]

var isCreating = false;
var isEditing = false;
var filaActual = '';
var fila = 0;
var columna = 0;

$(document).ready(function () {

	$('#btnNuevo').click(function (e) {
		if (!isCreating) {
			$('#btnNuevo').html('Ocultar');
			isCreating = true;
			mostrarCrud();
		} else {
			$('#btnNuevo').html('Nuevo');
			isCreating = false;
			ocultarCrud();
		}
	});
	$('#btnAgregar').click(function (e) {
		crear();
	});
	$('#btnEditar').click(function (e) {
		editar();
	});
	$("#btnAcceder").click(function (e){
		acceder();
	});
	$('#btnTriqui').click(function (e){
		iniciarTriqui();
	});


	contruirTabla();

});

function contruirTabla() {
	//$('#tableInfo').html(msg);
	var table = '';
	table += '<tr><th>Id</th><th>Nombre</th><th>Edad</th><th>Telefono</th><th>Opciones</th></tr>'

	listInfo.forEach(
		(listItem) => {
			table += '<tr>' + '<td>' + listItem.id + '</td>' + '<td>'
				+ listItem.nombre + '</td>' + '<td>' + listItem.edad + '</td>' + '<td>' + listItem.telefono + '</td>'
				+ '<td><button class="button button2" onclick="verRegistro(' + listItem.id + ')">Ver</button><button class="button button2" onclick="deleteItem(' + listItem.id + ')">Eliminar</button></td>' + '</tr>'
		}
	)

	$('#tableInfo').html(table);

}

function editar() {
	var idRegis = $('#id').val();
	var nombre = $('#nombre').val();
	var edad = $('#edad').val();
	var telefono = $('#telefono').val();

	itemToEdit = listInfo.find(
		(itemTo) => {
			return itemTo.id === Number(idRegis)
		}
	)

	listInfo[listInfo.indexOf(itemToEdit)] = {
		'id': Number(idRegis),
		'nombre': nombre,
		'edad': edad,
		'telefono': telefono
	}

	contruirTabla();

}

function crear() {
	var nombre = $('#nombre').val();
	var edad = $('#edad').val();
	var telefono = $('#telefono').val();


	listInfo.push(
		{
			'id': listInfo.length + 1,
			'nombre': nombre,
			'edad': edad,
			'telefono': telefono
		}
	)

	contruirTabla();
	mostrarCrud();
}

function deleteItem(item) {
	itemToDelete = listInfo.find(
		(itemTo) => {
			return itemTo.id === item
		}
	)

	listInfo.splice(listInfo.indexOf(itemToDelete), 1);

	contruirTabla();
}

function verRegistro(item) {
	itemToView = listInfo.find(
		(itemTo) => {
			return itemTo.id === item
		}
	)

	$('#cruMain').css('display', 'block');
	$('#btnEditar').css('display', 'block');
	$('#id').css('display', 'block');
	$('#btnAgregar').css('display', 'none');

	$('#id').val(itemToView.id);
	$('#nombre').val(itemToView.nombre);
	$('#edad').val(itemToView.edad);
	$('#telefono').val(itemToView.telefono);
}

function mostrarCrud() {
	$('#cruMain').css('display', 'block');
	$('#id').css('display', 'none');
	$('#btnEditar').css('display', 'none');
	$('#btnAgregar').css('display', 'block');
	$('#id').val('');
	$('#nombre').val('');
	$('#edad').val('');
	$('#telefono').val('');
}

function ocultarCrud() {
	$('#cruMain').css('display', 'none');
	$('#btnEditar').css('display', 'none');
}

function acceder() {
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		url: "http://localhost:8080/test/hello",
		success: function (data){console.log(data); console.log("Accedí"); },
		error: function (data){ console.log("No pude acceder") }
	});
}

function pintarMatrizInicial() {
    $.ajax({
        url: 'http://localhost:8080/test/obtenerInicioMatriz',
        contentType: "charset=utf-8",
        type: "GET",
        success: function (response) {
            drawTriqui(response);
        },
        error: function (responseError) {
            console.error(responseError);
        }
    });
}

function iniciarTriqui() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "http://localhost:8080/triqui/iniciar",
        success: function (data){
			console.log(JSON.parse(data));
			var matriz = JSON.parse(data);
			pintarTabla(matriz);
		},
        error: function (data){ console.log("No pude acceder") }
    });
}

function pintarTabla(matriz) {
	var tabla = '<tr>';
	
    for (var i = 0; i < matriz.length; i++) {
		for(var j = 0; j < matriz.length; j++){
			filaActual += '<td style="width: 100px; border: 1px solid black; text-align: center;"><a onclick="tocoItem(' + i + ',' + j + ')">' + matriz[i][j] + '</a></td>';
            
            
		}	
		tabla += filaActual + '</tr><tr>';
		filaActual = '';
    }

    $('#tablaPrincipal').html(tabla);
}

var turno = false;
var juegoTerminado = false;
function tocoItem(x, y) {
	console.log(x + " - " + y);
	if (!juegoTerminado) {
	$.ajax({
		url: 'http://localhost:8080/test/comprobar?x=' + x + '&y=' + y + '&raya=' + turno,
		data: "",
		contentType: "charset=utf-8",
		type: "GET",
		success: function (respuestaTriqui) {
			console.log("REspuesta: " + respuestaTriqui);
			if (respuestaTriqui === 'Gano-X') {
				console.log("Gano X");
				juegoTemrinado = !juegoTemrinado;
				getLastMatrix();
			} else if (respuestaTriqui === 'Gano-O') {
				console.log("Gano O");
				juegoTemrinado = !juegoTemrinado;
				pintarMatrizInicial();
			} else {
				contruirTabla();
			}
		},
		error: function (respuestaErrorTriqui) {
			console.error(respuestaErrorTriqui);
		}
	});
		turno = !turno;
	} else {
		console.log('Has perdido..! No insista');
	}
	
  }

